package pucrs.myflight.gui;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.SwingUtilities;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.GeoPosition;
import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import pucrs.myflight.modelo.Aeroporto;
import pucrs.myflight.modelo.CiaAerea;
import pucrs.myflight.modelo.Geo;
import pucrs.myflight.modelo.GerenciadorAeronaves;
import pucrs.myflight.modelo.GerenciadorAeroportos;
import pucrs.myflight.modelo.GerenciadorCias;
import pucrs.myflight.modelo.GerenciadorRotas;
import pucrs.myflight.modelo.Rota;

public class JanelaFX extends Application {
	final SwingNode mapkit = new SwingNode();

	private GerenciadorCias gerCias;
	private GerenciadorAeroportos gerAero;
	private GerenciadorRotas gerRotas;
	private GerenciadorAeronaves gerAvioes;
	private GerenciadorMapa gerenciador;
	private EventosMouse mouse;

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		setup();
		
		GeoPosition poa = new GeoPosition(-30.05, -51.18);
		gerenciador = new GerenciadorMapa(poa, GerenciadorMapa.FonteImagens.VirtualEarth);
		mouse = new EventosMouse();
		gerenciador.getMapKit().getMainMap().addMouseListener(mouse);
		gerenciador.getMapKit().getMainMap().addMouseMotionListener(mouse);
		
		createSwingContent(mapkit);
		BorderPane pane = new BorderPane();			
		GridPane leftPane = new GridPane();
		leftPane.setAlignment(Pos.CENTER);
		leftPane.setHgap(10);
		leftPane.setVgap(10);
		leftPane.setPadding(new Insets(20,20,20,20));
		pane.setCenter(mapkit);
		pane.setLeft(leftPane);
		Scene scene = new Scene(pane, 500, 500);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Mapas com JavaFX");
		primaryStage.show();
		
		//primeira consulta
		Button btnConsulta1 = new Button("Search");
		ChoiceBox<CiaAerea> escolheCia = new ChoiceBox<CiaAerea>();
		leftPane.add(btnConsulta1, 1,0);
		leftPane.add(escolheCia,0,0);
		for(CiaAerea c : gerCias){
			escolheCia.getItems().add(c);
		}
		btnConsulta1.setOnAction(e -> getChoiceConsulta1(escolheCia));
		
		//segunda consulta
		Button btnConsulta2 = new Button("Volume de tr�fego dos aeroportos");
		leftPane.add(btnConsulta2,0,2);
		btnConsulta2.setOnAction(e -> {Consulta2();});
		
		//terceira consulta
		Button btnConsulta3 = new Button("Rotas Aereas");
		ChoiceBox<Aeroporto> escolheOrig = new ChoiceBox<Aeroporto>();
		ChoiceBox<Aeroporto> escolheDest = new ChoiceBox<Aeroporto>();
		leftPane.add(btnConsulta3,1,3);
		leftPane.add(escolheOrig, 0,3);
		leftPane.add(escolheDest, 0,4);
		for(Aeroporto ar : gerAero){
			escolheOrig.getItems().add(ar);
			escolheDest.getItems().add(ar);
		}
		btnConsulta3.setOnAction(e -> getChoiceConsulta3(escolheOrig, escolheDest));
		
		//quarta consulta
		Button btnConsulta4 = new Button("Ver Aeroportos");
		ChoiceBox<Aeroporto> escolheAero = new ChoiceBox<Aeroporto>();
		leftPane.add(btnConsulta4,1,5);
		leftPane.add(escolheAero,0,5);
		for(Aeroporto ar : gerAero){
			escolheAero.getItems().add(ar);
		}
		btnConsulta4.setOnAction(e -> getChoiceConsulta4(escolheAero));
	}

    private void getChoiceConsulta4(ChoiceBox<Aeroporto> escolheAero){//consulta4
    	Aeroporto aero = escolheAero.getValue();
    	Consulta4(aero);
    }

	private void getChoiceConsulta3(ChoiceBox<Aeroporto> choiceOrig, ChoiceBox<Aeroporto> choiceDest){//consulta3
    	Aeroporto or = choiceOrig.getValue();
    	Aeroporto de = choiceDest.getValue();
    	Consulta3(or,de);
    }

	private void getChoiceConsulta1(ChoiceBox<CiaAerea> choicebox){//consulta1
    	CiaAerea cia = choicebox.getValue();
    	Consulta1(cia.getCodigo());
    }

	// Inicializando os dados aqui...
    private void setup() {

    	gerCias = new GerenciadorCias();
    	gerAero = new GerenciadorAeroportos();
    	gerAvioes = new GerenciadorAeronaves();
    	gerRotas = new GerenciadorRotas();
    	
		try {
			gerCias.carregaDados();
			gerAero.CarregaDados();
			gerRotas.CarregaDados(gerCias, gerAero, gerAvioes);
			//rg = new RoutesGraph(gerRotas);
		} catch (IOException e) {
			System.out.println("Impossivel ler arquivos.dat!");
			System.out.println("Msg: "+e);
			System.exit(1);
		}
	}
    
    private void consulta() {
    	
		// Para obter um ponto clicado no mapa, usar como segue:
    	GeoPosition pos = gerenciador.getPosicao();     

        // Lista para armazenar o resultado da consulta
        List<MyWaypoint> lstPoints = new ArrayList<>();
        
        // Exemplo de uso:
        Aeroporto poa = gerAero.buscarCodigo("POA");
        Aeroporto gru = gerAero.buscarCodigo("GRU"); 
        
        Geo locPoa = poa.getLocal();
        Geo locGru = gru.getLocal();
                       
        lstPoints.add(new MyWaypoint(Color.BLUE, poa.getNome(), locPoa, 10));
        lstPoints.add(new MyWaypoint(Color.RED, gru.getNome(), locGru, 5));
        
    	// Informa o resultado para o gerenciador
        gerenciador.setPontos(lstPoints);
        
        // Quando for o caso de limpar os traçados...
        //gerenciador.clear();
        
        // Exemplo: criando um tra�ado
        Tracado tr = new Tracado();        
        //Adicionando as mesmas localiza�oes de antes
        tr.addPonto(locPoa);
        tr.addPonto(locGru);
        tr.setWidth(4);       // largura do traçado
        tr.setCor(Color.RED); // cor do traçado
        
        // E adicionando o traçado...
        gerenciador.addTracado(tr);        
        gerenciador.getMapKit().repaint(); 
    }
    
    public void Consulta1(String s){
    	gerenciador.clear();
    	List<MyWaypoint> Pontos_Lista = new ArrayList<>();
    	String c = s; 
    	
    	for(Rota r: gerRotas){
    		if(r.getCia().getCodigo().equals(c)){
    			Pontos_Lista.add(new MyWaypoint(Color.BLUE, r.getOrigem().getNome(), r.getOrigem().getLocal(), 5));
    			Pontos_Lista.add(new MyWaypoint(Color.BLUE, r.getDestino().getNome(), r.getDestino().getLocal(), 5));
    			gerenciador.setPontos(Pontos_Lista);
    			Tracado tr = new Tracado();
    			tr.addPonto(r.getOrigem().getLocal());
    			tr.addPonto(r.getDestino().getLocal());
    			tr.setWidth(2);
    			tr.setCor(Color.RED);
    			gerenciador.addTracado(tr);
    		}
    	}
    	gerenciador.setPontos(Pontos_Lista);
    	gerenciador.getMapKit().repaint();
    }
    
    public void Consulta2(){
    	gerenciador.clear();
    	Map<Aeroporto, Integer> freqAero = new HashMap<Aeroporto, Integer>();
    	List<MyWaypoint> Pontos_Lista2 = new ArrayList<>();
    	for(Rota r: gerRotas){
    		Integer freq = freqAero.get(r.getOrigem());
    		freqAero.put(r.getOrigem(), (freq == null) ? 1 : freq + 1);
    	}    	
    	for(Aeroporto a: gerAero){
    		if (freqAero.containsKey(a)) {
    			Pontos_Lista2.add(new MyWaypoint(Color.RED,a.getNome(),a.getLocal(),freqAero.get(a)/100+2));
    			gerenciador.setPontos(Pontos_Lista2);
    			gerenciador.getMapKit().repaint();
    		}
    	}
    }
    
    public void Consulta3(Aeroporto orig, Aeroporto dest){
    	gerenciador.clear();
    }
    
    public void Consulta4(Aeroporto orig){    	
    	gerenciador.clear();
    }
	
	private class EventosMouse extends MouseAdapter {
		private int lastButton = -1;

		@Override
		public void mousePressed(MouseEvent e) {
			JXMapViewer mapa = gerenciador.getMapKit().getMainMap();
			GeoPosition loc = mapa.convertPointToGeoPosition(e.getPoint());
			// System.out.println(loc.getLatitude()+", "+loc.getLongitude());
			lastButton = e.getButton();
			// Botão 3: seleciona localização
			if (lastButton == MouseEvent.BUTTON3) {
				gerenciador.setPosicao(loc);				
				gerenciador.getMapKit().repaint();
			}
		}
	}
	
	private void createSwingContent(final SwingNode swingNode) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				swingNode.setContent(gerenciador.getMapKit());
			}
		});
	}
	
	public static void main(String[] args) {
		launch(args);			
	}
}
