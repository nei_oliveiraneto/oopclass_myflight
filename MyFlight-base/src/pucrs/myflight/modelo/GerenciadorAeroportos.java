package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

public class GerenciadorAeroportos implements Iterable<Aeroporto>{

	private ArrayList<Aeroporto> aeroportos;
	private HashMap<String, Aeroporto> airportsMap;
	public GerenciadorAeroportos() {
		aeroportos = new ArrayList<>();
		airportsMap = new HashMap<>();
	}
	
	public void adicionar(Aeroporto a) {
		aeroportos.add(a);
		airportsMap.put(a.getCodigo(), a);
	}
	
	public ArrayList<Aeroporto> listarTodos() {
		return new ArrayList<Aeroporto>(aeroportos);
	}
	
	public void ordenaNome() {
		Collections.sort(aeroportos);
	}
	
	public Aeroporto buscarCodigo(String codigo) {
		return airportsMap.get(codigo);
		/*for(Aeroporto a: aeroportos)
			if(a.getCodigo().equals(codigo))
				return a;
		return null;*/
	}
	
	public boolean containsAirport(String airportCode) {
		return airportsMap.containsKey(airportCode);
	}
	
	public void CarregaDados() throws IOException{
		Path path1 = Paths.get("airports.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path1, Charset.forName("utf8")))) {
			sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
			String header = sc.nextLine(); // pula cabeçalho
			String cod, nome, dest, lons, lats;
			double lat, lon;
			Geo loc;
			while (sc.hasNext()) {
				cod = sc.next();
				lats = sc.next();
				lons= sc.next();
				lat = Double.parseDouble(lats);
				lon = Double.parseDouble(lons);
				nome = sc.next();
				dest = sc.next();
				loc = new Geo(lat, lon);
				//System.out.format("%s - Localização:%s - %s%n", cod, loc, nome);
				Aeroporto apToBeAdded = new Aeroporto(cod, loc, nome);
				aeroportos.add(apToBeAdded);
				airportsMap.put(cod, apToBeAdded);
			}
		}
	}
	
	@Override
	public String toString() {
		StringBuilder aux = new StringBuilder();
		for(Aeroporto a: aeroportos)
			aux.append(a + "\n");			
		return aux.toString();
	}

	@Override
	public Iterator<Aeroporto> iterator() {
		return  new Iterator<Aeroporto>(){
			private int posicaoCorrente=0;
			
			@Override
			public boolean hasNext() {
				return posicaoCorrente < aeroportos.size();
			}
			@Override
			public Aeroporto next() {
				return aeroportos.get(posicaoCorrente++);
				
			}
		};
	}
}
