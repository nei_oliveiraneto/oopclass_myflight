package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

public class GerenciadorRotas implements Iterable<Rota>{

	private ArrayList<Rota> rotas;
	private HashMap<String, Rota> routesMap;
	
	public GerenciadorRotas() {
		rotas = new ArrayList<>();
		routesMap = new HashMap<>();
	}
	
	public void adicionar(Rota r) {
		rotas.add(r);
		routesMap.put(r.getOrigem().getCodigo()+" "+r.getDestino().getCodigo(), r);
	}
	
	public Rota get(Aeroporto apOrig, Aeroporto apDest) {
		return routesMap.get(apOrig.getCodigo()+" "+apDest.getCodigo());
	}
	
	public Rota get(String apOrig, String apDest) {
		return routesMap.get(apOrig+" "+apDest);
	}
	
	public ArrayList<Rota> listarTodas() {
		return new ArrayList<Rota>(rotas);
	}
	
	public int totalRotas() {
		return rotas.size();
	}
	
	public void ordenaCia() {
		//Collections.sort(rotas);
		rotas.sort((Rota r1, Rota r2)
			-> r1.getCia().getNome().compareTo(r2.getCia().getNome()));
	}
	
	public void ordenaOrigem() {
		rotas.sort( (Rota r1, Rota r2)
			-> r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome()));
	}
	
	public void ordenaOrigemCia() {
		/*
		rotas.sort( (Rota r1, Rota r2) -> {
			int r = r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome());
			if (r!=0)
				return r;
			return r1.getCia().getNome().compareTo(r2.getCia().getNome());
		});
		*/
		rotas.sort(Comparator.comparing((Rota r) -> r.getOrigem().getNome()).
				thenComparing(r -> r.getCia().getNome()));
	}
	
	public ArrayList<Rota> buscarOrigem(Aeroporto origem) {
		ArrayList<Rota> lista = new ArrayList<>();
		for(Rota r : rotas) {			
            //System.out.println(r.getOrigem().getCodigo());
			if(origem.getCodigo().equals(r.getOrigem().getCodigo()))
				lista.add(r);					
		}
		return lista;
	}
	
	public void CarregaDados(GerenciadorCias gerCias, GerenciadorAeroportos gerAero, GerenciadorAeronaves gerAvioes) throws IOException{
		Path path1 = Paths.get("routes.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path1, Charset.forName("utf8")))) {
			sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
			String header = sc.nextLine(); // pula cabe�alho
			String cias, equips, oris, ches;//,lixo, lixo2;
			CiaAerea cia = null ;
			Aeronave equip = null ;
			Aeroporto ori = null , che = null ;
			while (sc.hasNext()) {
				cias = sc.next();
				if (gerCias.containsCompany(cias)) {
					cia = gerCias.buscarCodigo(cias);
				}
				/*for(CiaAerea c : gerCias){
					if(cias.equals(c.getCodigo())){
						cia = c;
					}
				}*/
				oris = sc.next();
				if (gerAero.containsAirport(oris)) {
					ori = gerAero.buscarCodigo(oris);
				}
				/*for(Aeroporto a : gerAero){
					if(oris.equals(a.getCodigo())){
						ori = a;
					}
				}*/
				ches = sc.next();
				if (gerAero.containsAirport(ches)) {
					che = gerAero.buscarCodigo(ches);
				}
				/*for(Aeroporto a : gerAero){
					if(ches.equals(a.getCodigo())){
						che = a;
					}
				}*/
				//lixo = sc.next();
				//lixo2 = sc.next();
				equips = sc.next();
				if (gerAvioes.containsAircraft(equips)) {
					equip = gerAvioes.buscarCodigo(equips);
				}
				/*for(Aeronave a : gerAvioes){
					if(equips.equals(a.getCodigo())){
						equip = a;
						//System.out.println(a);
					}
				}*/
				//System.out.format("Cia: %s - Origem: %s - Destino: %s - Avi�o: %s%n", cia, ori, che, equip);
				
				Rota toBeAdded = new Rota(cia, ori, che, equip);
				rotas.add(toBeAdded);
				routesMap.put(toBeAdded.getOrigem().getCodigo()+" "+toBeAdded.getDestino().getCodigo(), toBeAdded);
			}
		}
	}
	
	@Override
	public String toString() {		
		StringBuilder aux = new StringBuilder();
		for(Rota r: rotas)
			aux.append(r + "\n");			
		return aux.toString();
	}

	@Override
	public Iterator<Rota> iterator() {
		return  new Iterator<Rota>(){
			private int posicaoCorrente=0;
			
			@Override
			public boolean hasNext() {
				return posicaoCorrente < rotas.size();
				//return false;
			}

			@Override
			public Rota next() {
				return rotas.get(posicaoCorrente++);
				
			}
		};
	}
}
