package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

public class GerenciadorAeronaves implements Iterable<Aeronave>{

	private ArrayList<Aeronave> aeronaves;
	private HashMap<String, Aeronave> aircraftsMap;

	public GerenciadorAeronaves() {
		aeronaves = new ArrayList<>();
		aircraftsMap = new HashMap<>();
	}
	
	public void ordenaCodigo() {
		//aeronaves.sort( (Aeronave a1, Aeronave a2)
		//		-> a1.getCodigo().compareTo(a2.getCodigo()));
		//aeronaves.sort(Comparator.comparing(a -> a.getCodigo()));
		aeronaves.sort(Comparator.comparing(Aeronave::getCodigo).reversed());
	}
	
	public void ordenaDescricao() {
		Collections.sort(aeronaves);
	}

	public void adicionar(Aeronave av) {
		aeronaves.add(av);
		aircraftsMap.put(av.getCodigo(), av);
	}
	
	public ArrayList<Aeronave> listarTodas() {
		return new ArrayList<Aeronave>(aeronaves);
	}

	public Aeronave buscarCodigo(String codigo) {
		return aircraftsMap.get(codigo);
		/*if (codigo == null)
			return null;
		for(Aeronave av: aeronaves)
			if(av.getCodigo().equals(codigo))
				return av;
		return null;*/
	}
	
	public boolean containsAircraft(String code) {
		return aircraftsMap.containsKey(code);
	}
	
	public void CarregaDados() throws IOException{
		Path path1 = Paths.get("equipment.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path1, Charset.forName("utf8")))) {
			sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
			String header = sc.nextLine(); // pula cabeçalho
			String cod, desc;
			int capac;
			while (sc.hasNext()) {
				cod = sc.next();
				desc = sc.next();
				capac = sc.nextInt();
				//System.out.format("%s - %s - %s%n", cod, desc, capac);
				Aeronave av = new Aeronave(cod, desc, capac);
				aeronaves.add(av);
				aircraftsMap.put(cod, av);
			}
		}
	}
	
	@Override
	public String toString() {
		StringBuilder aux = new StringBuilder();
		for(Aeronave av: aeronaves)
			aux.append(av.toString()+"\n");
		return aux.toString();
	}

	@Override
	public Iterator<Aeronave> iterator() {
		return  new Iterator<Aeronave>(){
			private int posicaoCorrente=0;
			
			@Override
			public boolean hasNext() {
				return posicaoCorrente < aeronaves.size();
			}

			@Override
			public Aeronave next() {
				return aeronaves.get(posicaoCorrente++);
				
			}
		};
	}
}
