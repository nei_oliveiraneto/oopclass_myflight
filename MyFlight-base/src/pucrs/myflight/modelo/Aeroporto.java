package pucrs.myflight.modelo;

public class Aeroporto implements Comparable<Aeroporto> {
	private String codigo;
	private String nome;
	private Geo loc;
	
	public Aeroporto(String codigo, Geo loc, String nome) {
		this.codigo = codigo;
		this.loc = loc;
		this.nome = nome;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Geo getLocal() {
		return loc;
	}

	@Override
	public int compareTo(Aeroporto o) {
		return nome.compareTo(o.nome);
	}
	
	@Override
	public String toString() {
		return codigo + " - " + nome;
	}
}
