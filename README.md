# README #


##O objetivo deste trabalho é implementar diversas consultas sobre a estrutura de dados, com a possibilidade de visualizar os resultados de forma gráfica, isto é, desenhados em um mapa. Um objetivo secundário é promover a utilização de controle de versão (git), gerenciando as contribuições dos integrantes do grupo de trabalho.##

O aplicativo deverá oferecer as seguintes consultas:

1. Desenhar todos os aeroportos onde uma determinada companhia aérea opera. Mostre também as rotas envolvidas.
 
2. Exibir uma estimativa de volume de tráfego de todos os aeroportos ou de um país específico. Deve-se explorar o tamanho e cor do ponto correspondente, para destacar aeroportos com maior ou menor tráfego.

3. Selecionar um aeroporto de origem e um de destino e mostrar todas as rotas possíveis entre os dois, considerando que haverá no máximo 3 conexões entre eles. Por exemplo, se houver as rotas POA->GRU->LIS->LHR e POA->GIG->LHR, esta consulta deve desenhar todas elas. É importante evitar loops, ou seja, não voltar para um aeroporto já percorrido. Ao selecionar uma rota, mostrar o tempo aproximado total de vôo e destacá-la no mapa com outra cor.

4. Selecionar um aeroporto de origem e mostrar todos os aeroportos que são alcançáveis até um determinado tempo de vôo (ex: 12 horas), com ou sem conexões1.